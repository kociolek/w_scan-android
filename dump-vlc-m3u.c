/*
 * Simple MPEG/DVB parser to achieve network/service information without initial tuning data
 *
 * Copyright (C) 2006, 2007, 2008, 2010, 2011, 2012 Winfried Koehler
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 * Or, point your browser to http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 *
 * The author can be reached at: handygewinnspiel AT gmx DOT de
 *
 * The project's page is http://wirbel.htpc-forum.de/w_scan/idx2.html
 */

/* 20120525 --wk */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "extended_frontend.h"
#include "scan.h"
#include "dump-vlc-m3u.h"
#include "lnb.h"

/******************************************************************************
 * NOTE: VLC-2.x.x seems to support DVB-S2 now, whereas VLC-1.x missed DVB-S2
 *       support.
 *
 * TODO:
 *       1) IMPLEMENT DVB-S2 SYNTAX FOR VLC.                        <- done.
 *       2) check all values for system, modulation, fec, ..        <- done.
 *       3) enshure UTF-8 compliance of service names (should be the easiest task) <- wrong. It's the hardest task. Names are converted by iconv to UTF8 and still probs..
 *****************************************************************************/
static int idx = 1;

#define T1 "\t"        
#define T2 "\t\t"
#define T3 "\t\t\t"
#define T4 "\t\t\t\t"
#define T5 "\t\t\t\t\t"
#define fprintf_tab0(v) fprintf(f, "%s", v)
#define fprintf_tab1(v) fprintf(f, "%s%s", T1, v)
#define fprintf_tab2(v) fprintf(f, "%s%s", T2, v)
#define fprintf_tab3(v) fprintf(f, "%s%s", T3, v)
#define fprintf_tab4(v) fprintf(f, "%s%s", T4, v)
#define fprintf_tab5(v) fprintf(f, "%s%s", T5, v)
#define fprintf_pair(p,v) fprintf(f, "%s=%c%s%c", p,34,v,34)

int vlc_inversion(int inversion) {
        switch(inversion) {
                case INVERSION_OFF:             return 0;
                case INVERSION_ON:              return 1;
                default:                        return 2;
                }
}

static const char * vlc_fec(int fec) {
  static const char * const code_rate_vlc[] = { "0", "1/2", "2/3", "3/4" ,"4/5", "5/6", "6/7", "7/8", "8/9", "9", "3/5", "9/10" }; /*"1/4", "1/3",*/ 
  if (fec > FEC_9_10) return ""; 
  return code_rate_vlc[fec];
}

static const char * vlc_modulation(int modulation) { 
  static const char * const mod_vlc[] = {"QPSK", "16QAM", "32QAM", "64QAM", "128QAM", "256QAM", "QAM", "8VSB", "16VSB", "8PSK", "16APSK", "32APSK", "DQPSK"}; 
  return  mod_vlc[modulation];
}

static const char * vlc_delsys (int guard_interval) {
        switch(guard_interval) {          
            //    case SYS_DVBC_ANNEX_A:    return "dvb-c";
                case SYS_DVBC_ANNEX_B:    return "dvb-c";
                case SYS_DVBT        :    return "dvb-t";
                case SYS_DVBS        :    return "dvb-s";
                case SYS_DVBS2       :    return "dvb-s2";
                case SYS_ISDBT       :    return "isdb-t";
                case SYS_ISDBS       :    return "isdb-s";
                case SYS_ISDBC       :    return "isdb-t";
                case SYS_ATSC        :    return "atsc";
                case SYS_DVBT2       :    return "dvb-t2";
            //    case SYS_DVBC_ANNEX_C:    return "dvb-c";
                default:                  return "unknown";
                }                        
}
  

int vlc_bandwidth (int bandwidth) {
        switch(bandwidth) {                  
                case 8000000:                   return 8;
                case 7000000:                   return 7;
                case 6000000:                   return 6;
                case 5000000:                   return 5;
                case 10000000:                  return 10;
                case 1712000:                   return 2;     // wrong in VLC. It's 1.712, not 2.
                default:                        return 0;
                }                         
}
                                       
int vlc_transmission (int transmission_mode) {
        switch(transmission_mode) {         
                case TRANSMISSION_MODE_2K:      return 2;
                case TRANSMISSION_MODE_8K:      return 8;
                case TRANSMISSION_MODE_4K:      return 4;
                case TRANSMISSION_MODE_1K:      return 1;
                case TRANSMISSION_MODE_16K:     return 16;
                case TRANSMISSION_MODE_32K:     return 32;
                default:                        return 0;
                }                         
}  

static const char * vlc_guard (int guard_interval) {
        switch(guard_interval) {          
                case GUARD_INTERVAL_1_32:       return "1/32";
                case GUARD_INTERVAL_1_16:       return "1/16";
                case GUARD_INTERVAL_1_8:        return "1/8";
                case GUARD_INTERVAL_1_4:        return "1/4";
                case GUARD_INTERVAL_1_128:      return "1/128";
                case GUARD_INTERVAL_19_128:     return "19/128";
                case GUARD_INTERVAL_19_256:     return "19/256";
                default:                        return 0;
                }                        
}  

int vlc_hierarchy (int hierarchy) {
        switch(hierarchy) {                  
                case HIERARCHY_NONE:            return 0;
                case HIERARCHY_1:               return 1;
                case HIERARCHY_2:               return 2;
                case HIERARCHY_4:               return 4;
                default:                        return 0;
                }                         
}

int vlc_rolloff (int rolloff) {
        switch(rolloff) {                  
                case ROLLOFF_35:                return 35;
                case ROLLOFF_20:                return 20;
                case ROLLOFF_25:                return 25;
                default:                        return 35;
                }                         
}


void vlc_xspf_prolog(FILE * f, uint16_t adapter, uint16_t frontend, struct w_scan_flags * flags, struct lnb_types_st * lnbp)
{
        fprintf_tab0("<?");
        fprintf_pair("xml version", "1.0");
        fprintf_tab0(" ");
        fprintf_pair("encoding", "UTF-8");
        fprintf_tab0("?>\n");
        fprintf_tab1("<playlist ");
        fprintf_pair("version", "1");
        fprintf_tab0(" ");
        fprintf_pair("xmlns", "http://xspf.org/ns/0/");
        fprintf_tab0(" ");
        fprintf_pair("xmlns:vlc", "http://www.videolan.org/vlc/playlist/ns/0/");
        fprintf_tab0(">\n");
        fprintf_tab2("<title>DVB Playlist</title>\n");
        fprintf_tab2("<trackList>\n");

        idx = 1;
}

void vlc_xspf_epilog(FILE *f)
{
        fprintf_tab2("</trackList>\n");

        fprintf_tab1("</playlist>\n");
}

void vlc_dump_dvb_parameters_as_xspf (FILE * f, struct tuning_parameters * p, struct w_scan_flags * flags, struct lnb_types_st * lnbp)
{
        switch (flags->scantype) {
        case SCAN_TERRCABLE_ATSC:
                fprintf (f, "%s", "atsc://");
                fprintf (f, "frequency=%i:",      p->frequency);
                fprintf (f, "modulation=%s",      vlc_modulation(p->u.atsc.modulation));
                /* NOTE: VLC is not honoring srate here. Most probably cable clear-qam tuning will fail
                 *       in several cases, where the frontend doesnt support 'AUTO'.
                 */
                break;
        case SCAN_CABLE:
                fprintf (f, "%s://",                    vlc_delsys(p->u.cable.delivery_system));
                fprintf (f, "frequency=%i:",            p->frequency);
                fprintf (f, "inversion=%i:",            vlc_inversion(p->inversion));
                /* VLC defaults to srate = 6875000 ??!
                 * NOTE: quite *uncommon* value for European DVB-C; Looks like a VLC bug. This value will
                 *       avoid DVB-C tuning for about 75% of European DVB-C users.
                 *       VLC should default to srate = 6900000 and QAM_64 or QAM_AUTO (QAM_AUTO is done.)
                 */
                fprintf (f, "srate=%i:",                p->u.cable.symbol_rate); 
                fprintf (f, "fec=%s:",                  vlc_fec(p->u.cable.fec_inner));
                fprintf (f, "modulation=%s",            vlc_modulation(p->u.cable.modulation));
                break;
        case SCAN_TERRESTRIAL:
                fprintf (f, "%s://",                    vlc_delsys(p->u.terr.delivery_system));
                fprintf (f, "frequency=%i:",            p->frequency);
                fprintf (f, "inversion=%i:",            vlc_inversion(p->inversion));
                fprintf (f, "bandwidth=%i:",            vlc_bandwidth(p->u.terr.bandwidth));
                fprintf (f, "code-rate-hp=%s:",         vlc_fec(p->u.terr.code_rate_HP));
                fprintf (f, "code-rate-lp=%s:",         vlc_fec(p->u.terr.code_rate_LP));
                fprintf (f, "modulation=%s:",           vlc_modulation(p->u.terr.constellation));
                fprintf (f, "transmission=%i:",         vlc_transmission(p->u.terr.transmission_mode));
                fprintf (f, "guard=%s:",                vlc_guard(p->u.terr.guard_interval));
                fprintf (f, "hierarchy=%i",             vlc_hierarchy(p->u.terr.hierarchy_information));
                break;
        case SCAN_SATELLITE:
                /* NOTE: VLC (1.1.4, 20101113) doesnt seem to support DVB-S2 at all, DVB-S only. */
                /* NOTE: VLC         20140102) more than 3 years later.. 
                 *       now supports DVB API v5 and S2 - good. But still seems to miss Rotor/positioner, SCR and DISEQC support.
                 *       But at least they broke this xspf file format successfully several times in this time.
                 *       - Comma vs. Semicolon change
                 *       - VLC still dies if '&' occurs in channel name.
                 *       - obsoleting options
                 *       - still NO FILE DOCUMENTATION for this dvb xspf format. :-(
                 */
                fprintf (f, "%s://",                    vlc_delsys(p->u.sat.modulation_system));
                if (p->u.sat.modulation_system != SYS_DVBS) {
                   fprintf (f, "modulation=%s:",    vlc_modulation(p->u.sat.modulation_type));
                   fprintf (f, "rolloff=%i:",       vlc_rolloff(p->u.sat.rolloff));
                   }
                fprintf (f, "frequency=%i:",            p->frequency);
                fprintf (f, "inversion=%i:",            vlc_inversion(p->inversion));
                fprintf (f, "srate=%i:",                p->u.sat.symbol_rate);
                if (p->u.sat.modulation_system == SYS_DVBS)
                if (p->u.sat.fec_inner != FEC_AUTO)
                   fprintf (f, "fec=%s:",               vlc_fec(p->u.sat.fec_inner));
                switch (p->u.sat.polarization) {
                        case POLARIZATION_HORIZONTAL:
                        case POLARIZATION_CIRCULAR_LEFT:
                                fprintf (f, "polarization=H:");
                                break;
                        default:
                                fprintf (f, "polarization=V:");
                                break;                                
                        }
                fprintf (f, "tone=%i", p->frequency >= lnbp->switch_val);
                // "main access error: unsafe option "dvb-tone" has been ignored for security reasons"
                //     (uh.. No, this is not the NSA export function calling home, it's really needed)

                if ((flags->sw_pos & 0xF) < 0xF)
                        fprintf (f, ":satno=%i", flags->sw_pos & 0xF);

                break;
        default:
                fatal("Unknown scantype %d\n", flags->scantype);
        };
}

void vlc_dump_service_parameter_set_as_xspf (FILE * f,
                                struct service * s,
                                struct transponder * t,
                                struct w_scan_flags * flags,
                                struct lnb_types_st *lnbp)
{
        // restrict allowed chars.
        char buf[256];
        int i,j,len = s->service_name? strlen(s->service_name):0;
        for (i=0,j=0; i<len; i++) {
            if (((s->service_name[i] >= '0') && (s->service_name[i] <= '9')) ||
                ((s->service_name[i] >= 'a') && (s->service_name[i] <= 'z')) ||
                ((s->service_name[i] >= 'A') && (s->service_name[i] <= 'Z')) ||
                 (s->service_name[i] == '.'))
               buf[j++]=s->service_name[i];
            else {
               switch(s->service_name[i]) {
                  case '�':  {buf[j++]='a'; buf[j++]='e'; break;}
                  case '�':  {buf[j++]='o'; buf[j++]='e'; break;}
                  case '�':  {buf[j++]='u'; buf[j++]='e'; break;}
                  case '�':  {buf[j++]='A'; buf[j++]='e'; break;}
                  case '�':  {buf[j++]='O'; buf[j++]='e'; break;}
                  case '�':  {buf[j++]='U'; buf[j++]='e'; break;}
                  case '�':  {buf[j++]='s'; buf[j++]='s'; break;}
                  default: buf[j++]='_';
                  }
               
               } 
            }
        buf[j++]=0;

        fprintf_tab3("<track>\n");
        fprintf (f, "%s%s%.4d. %s%s\n", T4, "<title>", idx++, buf, "</title>");
        fprintf (f, "%s%s",             T4, "<location>");

        vlc_dump_dvb_parameters_as_xspf(f, &t->param, flags, lnbp);

        fprintf (f, "%s\n", "</location>");
        fprintf_tab4("<extension ");
        fprintf_pair("application", "http://www.videolan.org/vlc/playlist/0");
        fprintf_tab0(">\n");
        fprintf (f, "%s%s%d%s\n", T5, "<vlc:id>", idx, "</vlc:id>");
        fprintf (f, "%s%s%d%s\n", T5, "<vlc:option>program=", s->service_id, "</vlc:option>");
        fprintf_tab4("</extension>\n");
        fprintf_tab3("</track>\n");
}
